package com.rowest.moviesmvvmsample

import com.rowest.moviesmvvmsample.data.remote.model.Movie
import com.rowest.moviesmvvmsample.data.repository.MoviesRepository
import com.rowest.moviesmvvmsample.repository.FakeMoviesRepository
import com.rowest.moviesmvvmsample.ui.movie.MoviesViewModel
import com.rowest.moviesmvvmsample.util.Resource
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import org.junit.*
import org.junit.Assert.*

@ExperimentalCoroutinesApi
class MoviesViewModelTest : BaseTest() {
    private val fakeMoviesRepository = FakeMoviesRepository()
    private val moviesRepository = mockk<MoviesRepository>(relaxed = true)
    private lateinit var moviesViewModel: MoviesViewModel

    @Before
    fun setup() {
        moviesViewModel = MoviesViewModel(fakeMoviesRepository)
    }

    @Test
    fun init_setIsLoadingLiveDataToFalse() {
        moviesViewModel = MoviesViewModel(fakeMoviesRepository)
        assertEquals(false, moviesViewModel.getIsLoading().getOrAwaitValue())
    }

    @Test
    fun getMovies_return_empty_list() = runTest {
        moviesViewModel = MoviesViewModel(moviesRepository)
        coEvery { moviesRepository.getMoviesBySearch(any()) } returns Resource.Success(data = listOf())

        moviesViewModel.getMovies("mockk")
        val movies = moviesViewModel.getMoviesLiveData().getOrAwaitValue()

        coVerify {
            moviesRepository.getMoviesBySearch(any())

            assertNotNull(movies)
            assertEquals(emptyList<Movie>(), movies)
        }
    }

    @Test
    fun getMovies_return_one_element() = runTest {
        val movie = mockk<Movie>()
        moviesViewModel = MoviesViewModel(moviesRepository)
        coEvery { moviesRepository.getMoviesBySearch(any()) } returns Resource.Success(data = listOf(movie))

        moviesViewModel.getMovies("mockk")
        val movies = moviesViewModel.getMoviesLiveData().getOrAwaitValue()

        coVerify {
            moviesRepository.getMoviesBySearch(any())
        }
        assertNotNull(movies)
        assertEquals(1, movies.size)
    }

    @Test
    fun getMovies_return_dummy_list_from_FakeRepository() = runTest {
        val moviesList = listOf(
            Movie(
                "The Matrix",
                "1999",
                "imdb123534",
                "movie",
                "https://www.themoviedb.org/t/p/original/kKaarELGdPv51vGj8R000azCJDU.jpg"
            ),
            Movie(
                "Harry Potter and the Sorcerer's Stone",
                "2001",
                "tt0241527",
                "movie",
                "https://www.filmonpaper.com/wp-content/uploads/2011/07/HarryPotterAndTheSorcerersStone_onesheet_USA_DrewStruzan-16.jpg"
            )
        )
        fakeMoviesRepository.setDummyList(moviesList)

        moviesViewModel.getMovies("mockk")
        val movies = moviesViewModel.getMoviesLiveData().getOrAwaitValue()

        println(movies)
        assertNotNull(movies)
        assertEquals(2, movies.size)
    }

    @Test
    fun getMovies_emulate_network_error_returns_empty_list() = runTest {
        fakeMoviesRepository.setShouldReturnNetworkError(true)

        moviesViewModel.getMovies("mock")
        val movies = moviesViewModel.getMoviesLiveData().getOrAwaitValue()

        assertEquals(emptyList<Movie>(), movies)
    }

    @After
    fun afterTests() {
        fakeMoviesRepository.setShouldReturnNetworkError(false)
    }
}