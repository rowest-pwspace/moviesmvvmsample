package com.rowest.moviesmvvmsample

import com.rowest.moviesmvvmsample.data.remote.model.MovieDetails
import com.rowest.moviesmvvmsample.data.remote.model.MoviesResponse
import com.rowest.moviesmvvmsample.data.remote.service.MoviesService
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody
import okhttp3.ResponseBody.Companion.toResponseBody
import retrofit2.HttpException
import retrofit2.Response

import java.lang.Exception

class FakeMoviesService : MoviesService {
    var isConnected = true
    var isNetworkException = false

    override suspend fun getMoviesBySearch(searchText: String): MoviesResponse {
        return when (isConnected) {
            true -> MoviesResponse(listOf(), 1)
            false -> if (isNetworkException) {
                throw HttpException(
                    Response.error<ResponseBody>(500, "".toResponseBody("plain/text".toMediaTypeOrNull()))
                )
            } else {
                throw Exception("Error42")
            }
        }
    }

    override suspend fun getMovieInfoByIMDB(imdbID: String): MovieDetails {
        return when (isConnected) {
            true -> MovieDetails(
                "Dummy Movie",
                "year",
                "imdbID123",
                "series",
                "https://google.com",
                "genre",
                "Director",
                "plot",
                "country",
                4.5f
            )
            false -> if (isNetworkException) {
                throw HttpException(
                    Response.error<ResponseBody>(500, "".toResponseBody("plain/text".toMediaTypeOrNull()))
                )
            } else {
                throw Exception("Error42")
            }
        }
    }
}