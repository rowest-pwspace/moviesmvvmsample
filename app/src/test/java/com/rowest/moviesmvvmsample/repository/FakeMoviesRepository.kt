package com.rowest.moviesmvvmsample.repository

import com.rowest.moviesmvvmsample.data.remote.model.Movie
import com.rowest.moviesmvvmsample.data.remote.model.MovieDetails
import com.rowest.moviesmvvmsample.data.repository.MoviesRepository
import com.rowest.moviesmvvmsample.util.Resource

class FakeMoviesRepository(private var shouldReturnNetworkError: Boolean = false) : MoviesRepository {
    private var moviesList = listOf<Movie>()

    override suspend fun getMoviesBySearch(searchText: String): Resource<List<Movie>> {
        return if (shouldReturnNetworkError) {
            Resource.Error(errorMsg = "Test Error42")
        } else {
            Resource.Success(data = moviesList)
        }
    }

    override suspend fun getMovieByIMDB(imdbID: String): Resource<MovieDetails> {
        TODO("Not yet implemented")
    }

    fun setShouldReturnNetworkError(value: Boolean) {
        shouldReturnNetworkError = value
    }

    fun setDummyList(movies: List<Movie>) {
        moviesList = movies
    }
}