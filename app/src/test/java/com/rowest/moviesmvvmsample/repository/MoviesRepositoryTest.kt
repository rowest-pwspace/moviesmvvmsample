package com.rowest.moviesmvvmsample.repository

import com.rowest.moviesmvvmsample.BaseTest
import com.rowest.moviesmvvmsample.FakeMoviesService
import com.rowest.moviesmvvmsample.data.remote.model.Movie
import com.rowest.moviesmvvmsample.data.repository.MoviesRepository
import com.rowest.moviesmvvmsample.data.repository.MoviesRepositoryImpl
import io.mockk.coVerify
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.any

@ExperimentalCoroutinesApi
class MoviesRepositoryTest : BaseTest() {
    lateinit var moviesRepository: MoviesRepository
    private val moviesService = FakeMoviesService()

    @Before
    fun setup() {
        moviesRepository = MoviesRepositoryImpl(moviesService)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getMoviesBySearch_returnEmptyResults() {
        moviesService.isConnected = true

        runTest {
            val moviesBySearch = moviesRepository.getMoviesBySearch("")

            assertNull(moviesBySearch.errorMessage)
            assertEquals(listOf<Movie>(), moviesBySearch.data)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getMoviesBySearch_return_general_error() {
        moviesService.isConnected = false

        runTest {
            val response = moviesRepository.getMoviesBySearch("")
            println(response.errorMessage)
            assertNotNull(response.errorMessage)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getMoviesBySearch_return_500_HttpException() {
        moviesService.isConnected = false
        moviesService.isNetworkException = true

        runTest {
            val response = moviesRepository.getMoviesBySearch("")
            println(response.errorMessage)

            assertNotNull(response.errorMessage)
            assertEquals(true, response.errorMessage?.startsWith("500 HttpException"))
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getMovieByIMDB_return_MovieDetails_from_FakeMovieService() {
        moviesService.isConnected = true

        runTest {
            val movieDetails = moviesRepository.getMovieByIMDB("")

            println(movieDetails.data)
            assertNull(movieDetails.errorMessage)
            assertNotNull(movieDetails.data)
        }
    }
}