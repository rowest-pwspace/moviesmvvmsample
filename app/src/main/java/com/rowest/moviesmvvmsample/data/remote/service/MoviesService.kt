package com.rowest.moviesmvvmsample.data.remote.service

import com.rowest.moviesmvvmsample.data.remote.API
import com.rowest.moviesmvvmsample.data.remote.model.MovieDetails
import com.rowest.moviesmvvmsample.data.remote.model.MoviesResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesService {
    @GET(API.Movies.MOVIES_URL)
    suspend fun getMoviesBySearch(@Query("s") searchText: String): MoviesResponse

    @GET(API.Movies.MOVIES_URL)
    suspend fun getMovieInfoByIMDB(@Query("i") imdbID: String): MovieDetails
}