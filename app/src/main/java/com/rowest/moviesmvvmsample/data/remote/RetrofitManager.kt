package com.rowest.moviesmvvmsample.data.remote


import com.rowest.moviesmvvmsample.BuildConfig
import com.rowest.moviesmvvmsample.data.remote.service.MoviesService
import com.rowest.moviesmvvmsample.util.NetworkLoggingSettings
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitManager {

    private fun getInstance(): Retrofit {
        var builder: OkHttpClient.Builder = OkHttpClient.Builder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
        builder = NetworkLoggingSettings.configOkHttpClient(builder)
        builder.addInterceptor(Interceptor { chain ->
            val original = chain.request();
            val requestBuilder: Request.Builder = original.newBuilder()
            requestBuilder.url(original.url.newBuilder().addQueryParameter("apikey", BuildConfig.API_KEY).build())
            val request = requestBuilder.build()
            val response = chain.proceed(request)
            response
        })

        return Retrofit.Builder()
            .baseUrl(API.BASE_URL)
            .client(builder.build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val moviesService: MoviesService by lazy {
        getInstance().create(MoviesService::class.java)
    }
}