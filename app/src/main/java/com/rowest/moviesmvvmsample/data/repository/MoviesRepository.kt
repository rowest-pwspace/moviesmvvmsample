package com.rowest.moviesmvvmsample.data.repository

import com.rowest.moviesmvvmsample.data.remote.model.Movie
import com.rowest.moviesmvvmsample.data.remote.model.MovieDetails
import com.rowest.moviesmvvmsample.util.Resource

interface MoviesRepository {
    suspend fun getMoviesBySearch(searchText: String): Resource<List<Movie>>

    suspend fun getMovieByIMDB(imdbID: String): Resource<MovieDetails>
}