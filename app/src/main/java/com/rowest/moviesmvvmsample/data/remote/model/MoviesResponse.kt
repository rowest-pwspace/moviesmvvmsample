package com.rowest.moviesmvvmsample.data.remote.model

import com.google.gson.annotations.SerializedName

data class MoviesResponse(
    @SerializedName("Search")
    val searchResults: List<Movie>?,
    val totalResults: Int
)
