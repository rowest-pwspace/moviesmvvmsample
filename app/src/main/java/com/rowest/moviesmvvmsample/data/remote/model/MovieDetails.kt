package com.rowest.moviesmvvmsample.data.remote.model

import com.google.gson.annotations.SerializedName

data class MovieDetails(
    @SerializedName("Title")
    val title: String,
    @SerializedName("Year")
    val year: String?,
    @SerializedName("imdbID")
    val imdbID: String?,
    @SerializedName("Type")
    val type: String?,
    @SerializedName("Poster")
    val posterUrl: String?,
    @SerializedName("Genre")
    val genre: String?,
    @SerializedName("Director")
    val director: String?,
    @SerializedName("Plot")
    val plot: String?,
    @SerializedName("Country")
    val country: String?,
    @SerializedName("imdbRating")
    val imdbRating: Float?
)
