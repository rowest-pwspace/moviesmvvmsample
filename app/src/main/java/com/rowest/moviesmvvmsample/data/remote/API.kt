package com.rowest.moviesmvvmsample.data.remote

object API {
    const val BASE_URL = "http://omdbapi.com/"

    object Movies {
        const val MOVIES_URL: String = BASE_URL
    }
}