package com.rowest.moviesmvvmsample.data.repository

import com.rowest.moviesmvvmsample.data.remote.model.Movie
import com.rowest.moviesmvvmsample.data.remote.model.MovieDetails
import com.rowest.moviesmvvmsample.data.remote.service.MoviesService
import com.rowest.moviesmvvmsample.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException
import java.lang.Exception

class MoviesRepositoryImpl(private val moviesApiService: MoviesService) : MoviesRepository {

    override suspend fun getMoviesBySearch(searchText: String): Resource<List<Movie>> {
        return withContext(Dispatchers.IO) {
            try {
                val movies = moviesApiService.getMoviesBySearch(searchText)
                Resource.Success(data = movies.searchResults)
            } catch (e: IOException) {
                Resource.Error(errorMsg = "IOException: ${e.message}")
            } catch (e: HttpException) {
                Resource.Error(errorMsg = "${e.code()} HttpException: ${e.message}")
            } catch (e: Exception) {
                Resource.Error(errorMsg = "Exception: ${e.message}")
            }
        }
    }

    override suspend fun getMovieByIMDB(imdbID: String): Resource<MovieDetails> {
        return withContext(Dispatchers.IO) {
            try {
                val movieDetails = moviesApiService.getMovieInfoByIMDB(imdbID)
                Resource.Success(data = movieDetails)
            } catch (e: IOException) {
                Resource.Error(errorMsg = "IOException: ${e.message}")
            } catch (e: HttpException) {
                Resource.Error(errorMsg = "HttpException: ${e.message}")
            } catch (e: Exception) {
                Resource.Error(errorMsg = "Exception: ${e.message}")
            }
        }
    }
}