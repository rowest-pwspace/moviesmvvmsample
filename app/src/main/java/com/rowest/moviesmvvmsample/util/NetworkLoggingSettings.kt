package com.rowest.moviesmvvmsample.util

import android.content.Context

import com.facebook.stetho.Stetho
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.rowest.moviesmvvmsample.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

object NetworkLoggingSettings {

    fun init(context: Context) {
        if (BuildConfig.DEBUG) {
            Stetho.initialize(
                Stetho
                    .newInitializerBuilder(context)
                    .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(context))
                    .build()
            )
        }
    }

    fun configOkHttpClient(okClientBuilder: OkHttpClient.Builder): OkHttpClient.Builder {
        return if (BuildConfig.DEBUG) {
            okClientBuilder
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.HEADERS))
                .addNetworkInterceptor(StethoInterceptor())
        } else okClientBuilder
    }
}