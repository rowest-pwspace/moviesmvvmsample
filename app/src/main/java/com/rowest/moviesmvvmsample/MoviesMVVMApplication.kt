package com.rowest.moviesmvvmsample

import android.app.Application
import com.rowest.moviesmvvmsample.util.NetworkLoggingSettings

class MoviesMVVMApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        NetworkLoggingSettings.init(this)
    }
}