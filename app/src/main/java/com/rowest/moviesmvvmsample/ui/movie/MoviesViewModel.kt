package com.rowest.moviesmvvmsample.ui.movie

import androidx.lifecycle.*
import com.rowest.moviesmvvmsample.data.remote.model.Movie
import com.rowest.moviesmvvmsample.data.repository.MoviesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MoviesViewModel(
    private val moviesRepository: MoviesRepository
) : ViewModel() {
    private val isLoading = MutableLiveData<Boolean>()
    private val moviesLiveData = MutableLiveData<List<Movie>>()
    private var lastQuery: String = ""

    init {
        isLoading.value = false
    }

    fun getIsLoading(): LiveData<Boolean?> {
        return isLoading
    }

    fun getMoviesLiveData(): LiveData<List<Movie>> {
        return moviesLiveData
    }

    fun getMovies(query: String) {
        if (query.equals(lastQuery, true)) {
            return
        }
        lastQuery = query
        isLoading.value = true

        viewModelScope.launch {
            val result = withContext(Dispatchers.IO) {
                moviesRepository.getMoviesBySearch(query)
            }
            if (!result.data.isNullOrEmpty()) {
                moviesLiveData.value = result.data!!
                isLoading.value = false
            } else {
                moviesLiveData.value = emptyList()
                isLoading.value = false
            }
        }
    }

    class MoviesViewModelFactory(
        private val moviesRepository: MoviesRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(MoviesViewModel::class.java)) {
                return MoviesViewModel(moviesRepository) as T
            }
            throw IllegalArgumentException("Unknown class")
        }
    }

}