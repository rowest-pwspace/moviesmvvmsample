package com.rowest.moviesmvvmsample.ui.movie.details

import androidx.lifecycle.*
import com.rowest.moviesmvvmsample.data.remote.model.MovieDetails
import com.rowest.moviesmvvmsample.data.repository.MoviesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MovieDetailsViewModel(private val moviesRepository: MoviesRepository) : ViewModel() {
    private val isLoading = MutableLiveData<Boolean>()
    private val movieDetailsLiveData = MutableLiveData<MovieDetails>()

    fun getIsLoading(): LiveData<Boolean?> {
        return isLoading
    }

    fun getMovieDetailsLiveData(): LiveData<MovieDetails> {
        return movieDetailsLiveData
    }

    fun getMovieDetails(imdbID: String) {
        isLoading.value = true
        viewModelScope.launch {
            val result = withContext(Dispatchers.IO) {
                moviesRepository.getMovieByIMDB(imdbID)
            }
            if (result.data != null) {
                movieDetailsLiveData.value = result.data!!
                isLoading.value = false
                return@launch
            } else {
                isLoading.value = false
                return@launch
            }
        }
    }

    class MovieDetailsViewModelFactory(private val moviesRepository: MoviesRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(MovieDetailsViewModel::class.java)) {
                return MovieDetailsViewModel(moviesRepository) as T
            }
            throw IllegalArgumentException("Unknown class")
        }
    }
}