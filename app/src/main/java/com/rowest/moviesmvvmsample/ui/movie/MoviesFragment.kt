package com.rowest.moviesmvvmsample.ui.movie


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.*
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rowest.moviesmvvmsample.R
import com.rowest.moviesmvvmsample.data.remote.RetrofitManager
import com.rowest.moviesmvvmsample.data.remote.model.Movie
import com.rowest.moviesmvvmsample.data.repository.MoviesRepositoryImpl
import com.rowest.moviesmvvmsample.databinding.MoviesFragmentBinding
import com.rowest.moviesmvvmsample.ui.adapter.MoviesRecyclerViewAdapter
import com.rowest.moviesmvvmsample.ui.movie.details.MovieDetailsFragment
import com.rowest.moviesmvvmsample.util.textChanges
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@FlowPreview
class MoviesFragment : Fragment() {
    private var _binding: MoviesFragmentBinding? = null
    private val binding get() = _binding!!
    private val moviesList = ArrayList<Movie>()
    private val viewModel: MoviesViewModel by viewModels {
        val repository = MoviesRepositoryImpl(RetrofitManager.moviesService)
        MoviesViewModel.MoviesViewModelFactory(repository)
    }
    private lateinit var moviesRecyclerViewAdapter: MoviesRecyclerViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = MoviesFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupMoviesRecyclerView()

        watchUserInput()
        observeDataChanges()
    }

    private fun updateUI(query: String) {
        if (query.isNotEmpty() && query.length >= 2) {
            viewModel.getMovies(query)
        }
    }

    private fun observeDataChanges() {
        with(viewModel) {
            getMoviesLiveData().observe(viewLifecycleOwner, ::onMoviesUpdated)
            getIsLoading().observe(viewLifecycleOwner, ::onLoadingStateUpdated)
        }
    }

    private fun onMoviesUpdated(movies: List<Movie>) {
        if (movies.isNotEmpty()) {
            moviesRecyclerViewAdapter.submitList(movies)
        } else {
            moviesRecyclerViewAdapter.clearData()
        }
    }

    private fun onLoadingStateUpdated(isLoading: Boolean?) {
        if (isLoading != null && isLoading) {
            binding.progressIndicator.show()
        } else {
            binding.progressIndicator.hide()
        }
    }

    private fun setupMoviesRecyclerView() {
        moviesRecyclerViewAdapter = MoviesRecyclerViewAdapter(requireContext(), moviesList)
        binding.resultsRecyclerView.adapter = moviesRecyclerViewAdapter
        binding.resultsRecyclerView.layoutManager = GridLayoutManager(
            context, 1, GridLayoutManager.VERTICAL, false
        )
        moviesRecyclerViewAdapter.onItemClick = { movie ->
            replaceFragment(movie.imdbID)
        }
    }

    private fun replaceFragment(imdbID: String?) {
        parentFragmentManager.commit {
            setReorderingAllowed(true)
            val bundle = bundleOf(MoviesActivity.IMDB_ID_EXTRA to imdbID)
            replace<MovieDetailsFragment>(R.id.fragment_container, args = bundle)
            addToBackStack(null)
        }
    }

    private fun watchUserInput() {
        binding.searchEditText.textChanges()
            .filterNot { it.isNullOrBlank() }
            .debounce(700)
            .onEach { updateUI(it.toString()) }
            .launchIn(lifecycleScope)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}