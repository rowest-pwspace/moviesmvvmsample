package com.rowest.moviesmvvmsample.ui.movie.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.rowest.moviesmvvmsample.R
import com.rowest.moviesmvvmsample.data.remote.RetrofitManager
import com.rowest.moviesmvvmsample.data.remote.model.MovieDetails
import com.rowest.moviesmvvmsample.data.repository.MoviesRepositoryImpl
import com.rowest.moviesmvvmsample.databinding.MovieDetailsFragmentBinding
import com.rowest.moviesmvvmsample.ui.movie.MoviesActivity
import java.util.*

class MovieDetailsFragment : Fragment() {
    private var _binding: MovieDetailsFragmentBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MovieDetailsViewModel by viewModels {
        val repository = MoviesRepositoryImpl(RetrofitManager.moviesService)
        MovieDetailsViewModel.MovieDetailsViewModelFactory(repository)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = MovieDetailsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val imdbID = arguments?.getString(MoviesActivity.IMDB_ID_EXTRA, "")
        if (imdbID.isNullOrEmpty()) {
            Toast.makeText(context, getText(R.string.error_loading_movie_details), Toast.LENGTH_LONG).show()
            return
        }

        viewModel.getIsLoading().observe(viewLifecycleOwner) { isLoading ->
            if (isLoading != null && isLoading) {
                binding.progressIndicator.show()
            } else {
                binding.progressIndicator.hide()
            }
        }

        with(viewModel) {
            getMovieDetailsLiveData().observe(viewLifecycleOwner, ::onMovieDetailsUpdated)
            getMovieDetails(imdbID)
        }
    }

    private fun onMovieDetailsUpdated(movieDetails: MovieDetails) {
        with(movieDetails) {
            Glide.with(this@MovieDetailsFragment).load(this.posterUrl).into(binding.posterImageView)
            binding.titleText.text = this.title
            binding.yearText.text = this.year
            binding.imdbText.text = this.imdbID
            binding.imdbRatingText.text = String.format(Locale.US, "%.1f", this.imdbRating)
            binding.typeText.text = this.type
            binding.genreText.text = this.genre
            binding.directorText.text = this.director
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}