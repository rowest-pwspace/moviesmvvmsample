package com.rowest.moviesmvvmsample.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rowest.moviesmvvmsample.R
import com.rowest.moviesmvvmsample.data.remote.model.Movie
import com.rowest.moviesmvvmsample.ui.base.BaseViewHolder

class MoviesRecyclerViewAdapter(
    private val context: Context,
    private val moviesList: MutableList<Movie>
) : RecyclerView.Adapter<BaseViewHolder<*>>() {
    var onItemClick: ((Movie) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return when (viewType) {
            VIEW_TYPE_EMPTY -> {
                val view = LayoutInflater.from(context)
                    .inflate(R.layout.empty_recycler_item, parent, false)
                EmptyViewHolder(view)
            }
            VIEW_TYPE_ITEM -> {
                val view = LayoutInflater.from(context)
                    .inflate(R.layout.movie_recycler_item, parent, false)
                MovieHolder(view)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        if (position != RecyclerView.NO_POSITION) {
            when (holder) {
                is EmptyViewHolder -> holder.bind(context.getString(R.string.no_data))
                is MovieHolder -> holder.bind(moviesList[position])
                else -> throw IllegalArgumentException("Invalid ViewHolder")
            }
        }
    }

    override fun getItemCount(): Int {
        return when (val count = moviesList.size) {
            0 -> 1
            else -> count
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (moviesList.isEmpty()) {
            VIEW_TYPE_EMPTY
        } else {
            VIEW_TYPE_ITEM
        }
    }

    fun clearData() {
        moviesList.clear()
        notifyDataSetChanged()
    }

    fun submitList(newMoviesList: List<Movie>) {
        val isEmpty = moviesList.size == 0
        val diffResult = DiffUtil.calculateDiff(MovieDiffUtilCallback(moviesList, newMoviesList))
        diffResult.dispatchUpdatesTo(this)
        moviesList.clear()
        moviesList.addAll(newMoviesList)
        if (isEmpty) {
            notifyItemChanged(moviesList.size)
        }
    }

    companion object {
        private const val VIEW_TYPE_EMPTY = 0
        private const val VIEW_TYPE_ITEM = 1
    }

    inner class MovieHolder(itemView: View) : BaseViewHolder<Movie>(itemView) {
        private val title: TextView = itemView.findViewById(R.id.title_text)
        private val year: TextView = itemView.findViewById(R.id.year_text)
        private val imdb: TextView = itemView.findViewById(R.id.imdb_text)
        private val type: TextView = itemView.findViewById(R.id.type_text)
        private val poster: ImageView = itemView.findViewById(R.id.poster_image_view)

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(moviesList[adapterPosition])
            }
        }

        override fun bind(item: Movie) {
            title.text = item.title
            year.text = item.year
            imdb.text = item.imdbID
            type.text = item.type
            Glide.with(context)
                .load(item.posterUrl)
                .placeholder(getDrawable(context, R.drawable.placeholder))
                .into(poster)
        }
    }

    inner class EmptyViewHolder(itemView: View) : BaseViewHolder<String>(itemView) {
        private val label: TextView = itemView.findViewById(R.id.label_text)

        override fun bind(item: String) {
            label.text = item
        }
    }
}

class MovieDiffUtilCallback(
    private val oldItems: List<Movie>,
    private val newItems: List<Movie>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition].imdbID.equals(newItems[newItemPosition].imdbID, true)
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition] == newItems[newItemPosition]
    }
}